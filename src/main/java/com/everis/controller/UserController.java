package com.everis.controller;


import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.everis.modelo.User;
import com.everis.services.UserService;
import com.everis.services.exception.NotFoundException;


@RestController
@RequestMapping(value="/users")
public class UserController {

	@Autowired
	private UserService service;
	

	@GetMapping
	public ResponseEntity<List<User>> listar() throws Exception {
		List<User> lista  =  service.listar();
		return new ResponseEntity<List<User>>(lista, HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<User> listarPorID(@PathVariable("id") String id) throws Exception {
		
		User obj = service.listarPorId(id);
		
		if(obj.getId()== null) {
			
			throw new NotFoundException("Id no encontrado " + id);
		}
		
		return new ResponseEntity<User>(obj, HttpStatus.OK);
	}
	
	
	
	/*
	@PostMapping
	public ResponseEntity<Paciente> registrar(@Valid @RequestBody Paciente p) {
		Paciente obj =  service.registrar(p);
		return new ResponseEntity<Paciente>(obj, HttpStatus.CREATED);
	} */
	

	
	@PostMapping
	public ResponseEntity<User> registrar(@Valid @RequestBody User u) throws Exception {
		User obj =  service.registrar(u);
		//localhost:8080/pacientes/2
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	
	
	@PutMapping
	public ResponseEntity<User> modificar(@Valid @RequestBody User u) throws Exception {
		User obj =  service.modificar(u);
		return new ResponseEntity<User>(obj, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") String id) throws Exception {
		
		User obj =  service.listarPorId(id);
		
		if(obj.getId()== null) {
			
			throw new NotFoundException("Id no encontrado " + id);
		}
		
		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
}
