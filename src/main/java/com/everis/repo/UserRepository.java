package com.everis.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.everis.modelo.User;

@Repository
public interface UserRepository extends MongoRepository<User,String>{

}
