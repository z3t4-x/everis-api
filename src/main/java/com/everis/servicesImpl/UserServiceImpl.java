package com.everis.servicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;


import com.everis.modelo.User;
import com.everis.repo.UserRepository;
import com.everis.services.UserService;



@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository repo;
	
	
	@Override
	public User registrar(User u) {
		return repo.save(u);
	}

	@Override
	public User modificar(User u) {
		return repo.save(u);
	}

	@Override
	public List<User> listar() {
		return repo.findAll();
	}


	@Override
	public User listarPorId(String id) throws Exception {
		Optional<User> op =  repo.findById(id);
		return op.isPresent() ? op.get() : new User();
	}




	
	@Override
	public void eliminar(String id) {
		repo.deleteById(id);
		
	}

	
	
}
