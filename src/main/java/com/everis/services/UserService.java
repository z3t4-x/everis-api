package com.everis.services;

import java.util.List;


import com.everis.modelo.User;


public interface UserService {

	User registrar(User u)throws Exception;
	User modificar(User u)throws Exception;
	List<User> listar()throws Exception;
	User listarPorId(String id)throws Exception;
	
	void eliminar(String id)throws Exception;

}
